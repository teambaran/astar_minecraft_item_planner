import json
import math
from collections import namedtuple, defaultdict, OrderedDict
from timeit import default_timer as time
from heapq import heappop, heappush
from time import sleep
Recipe = namedtuple('Recipe', ['name', 'check', 'effect', 'cost'])

tools = ['bench', 'wooden_pickaxe', 'wooden_axe', 'stone_axe', 'stone_pickaxe', 'iron_axe', 'iron_pickaxe', 'furnace']
make_tools = ['craft stone_axe at bench', 'craft bench','craft wooden_axe at bench','craft iron_axe at bench',
                    'craft furnace at bench','craft iron_pickaxe at bench','craft cart at bench','craft stone_pickaxe at bench','craft wooden_pickaxe at bench']

class State(OrderedDict):
    """ This class is a thin wrapper around an OrderedDict, which is simply a dictionary which keeps the order in
        which elements are added (for consistent key-value pair comparisons). Here, we have provided functionality
        for hashing, should you need to use a state as a key in another dictionary, e.g. distance[state] = 5. By
        default, dictionaries are not hashable. Additionally, when the state is converted to a string, it removes
        all items with quantity 0.

        Use of this state representation is optional, should you prefer another.
    """

    def __key(self):
        return tuple(self.items())

    def __hash__(self):
        return hash(self.__key())

    def __lt__(self, other):
        return self.__key() < other.__key()

    def copy(self):
        new_state = State()
        new_state.update(self)
        return new_state

    def __str__(self):
        return str(dict(item for item in self.items() if item[1] > 0))


def make_checker(rule):
    # Implement a function that returns a function to determine whether a state meets a
    # rule's requirements. This code runs once, when the rules are constructed before
    # the search is attempted.

    def check(state):
        
        # This code is called by graph(state) and runs millions of times.
        # Tip: Do something with rule['Consumes'] and rule['Requires'].
        if 'Requires' in rule:
            rule_requires = rule['Requires']
            for key in rule_requires:
                if not state[key]:
                    return False
                elif rule_requires[key] and state[key] < 1:
                    return False
        if 'Consumes' in rule:
            rule_consumes = rule['Consumes']
            for key in rule_consumes:
                if key not in state:
                    return False
                elif rule_consumes[key] > state[key]:
                    return False                
        return True

    return check


def make_effector(rule):
    # Implement a function that returns a function which transitions from state to
    # new_state given the rule. This code runs once, when the rules are constructed
    # before the search is attempted.

    def effect(state):
        # This code is called by graph(state) and runs millions of times
        # Tip: Do something with rule['Produces'] and rule['Consumes'].
        next_state = state.copy()
        if 'Produces' in rule:
            for item in rule['Produces']:
                next_state[item] += rule['Produces'][item]
        if 'Consumes' in rule:
            for item in rule['Consumes']:
                next_state[item] -= rule['Consumes'][item]
        return next_state

    return effect


def make_goal_checker(goal):
    # Implement a function that returns a function which checks if the state has
    # met the goal criteria. This code runs once, before the search is attempted.
    
    def is_goal(state):
        # This code is used in the search process and may be called millions of times.
        for item in goal:
            if not state[item] >= goal[item]:
                return False
        return True
    return is_goal


def graph(state):
    # Iterates through all recipes/rules, checking which are valid in the given state.
    # If a rule is valid, it returns the rule's name, the resulting state after application
    # to the given state, and the cost for the rule.
    for r in all_recipes:
        if r.check(state):
            yield (r.name, r.effect(state), r.cost)

def heuristic(state, action, next_state):
    # Implement your heuristic here!
    # need to know what are the tools needed to craft
    goal = Crafting['Goal']

    for tool in tools:
        if next_state[tool] > 1: #if the tool already exists, dont make another one
            return math.inf
        elif action in make_tools: #if an action produces a tool we dont have, make it
            return 0 
        elif tool in action: #otherwise make something using a tool if there aren't too many of them
            if next_state['iron_pickaxe'] == 1:
                if 'stone_pickaxe' in action or 'wooden_pickaxe' in action:
                    return math.inf
            elif next_state['iron_axe'] == 0:
                if 'stone_axe' in action or 'wooden_axe' in action:
                    return math.inf
            elif next_state['stone_pickaxe'] == 1:
                if 'wooden_pickaxe' in action:
                    return math.inf
            elif next_state['stone_axe'] == 1:
                if 'wooden_axe' in action:
                    return math.inf
            for item in next_state:
                if next_state[item] <= 20:
                    return 1
    for item in goal:
        new_quantity = next_state[item] + state[item]
        if next_state[item] > state[item] and not next_state[item] >= goal[item]:
            return 1

    return Crafting['Recipes'][action]['Time'] * 1000

def search(graph, state, is_goal, limit, heuristic):

    start_time = time()
    queue = [(0, state)]
    cost = {}
    cost[state] = 0
    backpointers = {}
    backpointers[state] = None
    actions = {}
    actions[state] = None
    visited_states = set()

    # Implement your search here! Use your heuristic here!
    # When you find a path to the goal return a list of tuples [(state, action)]
    # representing the path. Each element (tuple) of the list represents a state
    # in the path and the action that took you to this state

    while time() - start_time < limit:
        priority, current_state = heappop(queue)

        #if we happen to reach the goal then return path and print the states and time 
        if is_goal(current_state):
            path = [(current_state, actions[current_state])]
            current_back_state = backpointers[current_state]
            while current_back_state is not None:
                path.insert(0,(current_back_state, actions[current_back_state]))
                current_back_state = backpointers[current_back_state]
            print('total number of states visited: ', len(visited_states))
            print('number of actions taken: ' + str(len(path)-1))
            print('cost was:', str(cost[current_state]))
            print(time() - start_time, "seconds.")
            print(current_state)
            return path 

        #go to next state and use heuristic to calcuate costs
        for next_action, next_state, next_cost in graph(current_state):
            total_cost = cost[current_state] + next_cost
            if (next_state not in cost or total_cost < cost[next_state]) and next_state not in visited_states:
                cost[next_state] = total_cost
                backpointers[next_state] = current_state
                actions[next_state] = next_action
                visited_states.add(next_state)
                priority = total_cost + heuristic(current_state, next_action, next_state)
                heappush(queue, (priority, next_state))

    # Failed to find a path
    print(queue[0])
    print(actions[queue[0][1]])
    print(time() - start_time, 'seconds.')
    print("Failed to find a path from", state, 'within time limit.')
    return None

if __name__ == '__main__':
    with open('Crafting.json') as f:
        Crafting = json.load(f)

    # # List of items that can be in your inventory:
    # print('All items:', Crafting['Items'])
    #
    # # List of items in your initial inventory with amounts:
    # print('Initial inventory:', Crafting['Initial'])
    #
    # # List of items needed to be in your inventory at the end of the plan:
    # print('Goal:',Crafting['Goal'])
    #
    # # Dict of crafting recipes (each is a dict):
    # print('Example recipe:','craft stone_pickaxe at bench ->',Crafting['Recipes']['craft stone_pickaxe at bench'])

    # Build rules
    all_recipes = []
    for name, rule in Crafting['Recipes'].items():
        checker = make_checker(rule)
        effector = make_effector(rule)
        recipe = Recipe(name, checker, effector, rule['Time'])
        all_recipes.append(recipe)

    # Create a function which checks for the goal
    is_goal = make_goal_checker(Crafting['Goal']) 

    # Initialize first state from initial inventory
    state = State({key: 0 for key in Crafting['Items']})
    state.update(Crafting['Initial'])

    # Search for a solution
    resulting_plan = search(graph, state, is_goal, 30, heuristic)

    if resulting_plan:
        # Print resulting plan
        for state, action in resulting_plan:
            print('\t',state)
            print(action)
